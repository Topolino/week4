#Name: Joey Cranberry
#Date: September 22, 2015
#Project: Quiz Two
#Each Problem is labled and commented out with three apostraphies

#Problem 1
#-------------------------- 
'''
picFile = pickAFile()
picture = makePicture(picFile)
def deleteBlue(picture):
  for pixel in getPixels(picture):
    blueValue = getBlue(pixel)
    if(blueValue < 150):
      setColor(pixel, white)
deleteBlue(picture)
show(picture)
'''
#Problem 2
#--------------------------
'''
picFile = pickAFile()
picture = makePicture(picFile)
def changeColors(picture):
  for pixel in getPixels(picture):
    redValue = getRed(pixel) / 2
    setRed(pixel, redValue)
    greenValue = getGreen(pixel) / 2
    setGreen(pixel, greenValue)
    blueValue = getBlue(pixel) * 2
    setBlue(pixel, blueValue)
changeColors(picture)
show(picture)
'''
#Problem 3
#--------------------------
'''
picFile = pickAFile()
picture = makePicture(picFile)
def changeColors(picture):
  for pixel in getPixels(picture):
    redValue = getRed(pixel) * 2
    setRed(pixel, redValue)
    greenValue = getGreen(pixel) / 2
    setGreen(pixel, greenValue)
    blueValue = getBlue(pixel) / 2
    setBlue(pixel, blueValue)
changeColors(picture)
show(picture)
'''
#Problem 4
#--------------------------
'''
picFile = pickAFile()
picture = makePicture(picFile)
def grayScale(picture):
  for pixel in getPixels(picture):
    intensity = (getRed(pixel)+getGreen(pixel)+getBlue(pixel))/2
    setColor(pixel,makeColor(intensity,intensity,intensity))
def makeNegative(picture):
  for pixel in getPixels(picture):
    redC = getRed(pixel)
    greenC = getGreen(pixel)
    blueC = getBlue(pixel)
    negColor = makeColor(255-redC, 255-greenC, 255-blueC)
    setColor(pixel,negColor)
grayScale(picture)
show(picture)
makeNegative(picture)
show(picture)
'''
#Problem 5
#--------------------------
'''
picFile = pickAFile()
picture = makePicture(picFile)
def lightenPicture(picture):
  for pixel in getPixels(picture):
    redValue = getRed(pixel) + 75
    setRed(pixel, redValue)
    greenValue = getGreen(pixel) + 75
    setGreen(pixel, greenValue)
    blueValue = getBlue(pixel) + 75
    setBlue(pixel, blueValue)
lightenPicture(picture)
show(picture)
'''
#Problem 6
#--------------------------
'''
picFile = pickAFile()
picture = makePicture(picFile)
height = getHeight(picture) / 2
width = getWidth(picture)
x = 0
y = 0
def topHalfBlack(picture):
  for y in range(0,height):
    y += 1
    x = 0
    for x in range(0, width):
      pixel = getPixelAt(picture, x, y)
      setColor(pixel, black)
topHalfBlack(picture)
show(picture)
'''
#Problem 7
#--------------------------
'''
picFile = pickAFile()
picture = makePicture(picFile)

def flipPicture(picture):
  height = getHeight(picture)
  halfHeight = height/2
  width = getWidth(picture)
  
  for y in range(halfHeight, height):
    for x in range(0, width):
      newY = height - 1 - y
      pixel1 = getPixelAt(picture, x, y)
      pixel2 = getPixelAt(picture, x, newY)
      pixel1Color = getColor(pixel1)
      setColor(pixel2, pixel1Color)
      
flipPicture(picture)
show(picture)
'''